#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Visualization of DFFOO gacha rates.

"""

import numpy as np
import pandas as pd
from bokeh.core.enums import TooltipAttachment
from bokeh.io import output_file
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, HoverTool, Range1d, NumeralTickFormatter, Select, Panel, Tabs, Label
from bokeh.models.callbacks import CustomJS
from bokeh.models.tickers import FixedTicker, BasicTicker
from bokeh.plotting import figure, save
from scipy.special import binom

# Const
TITLE = 'DFFOO: Budget vs. Confidence Level (FR Era)'
URL = "https://ckhurewa.gitlab.io/dffoo-gacha-plot"
HEIGHT, WIDTH = 540, 1080
N_POINTS = 400  # smoother but larger file size.


def tooltip(caption, field):
    return f"""
<div style="height: 6px; line-height: 6px; text-align: center;">
  {caption} = <b>@{field}{{0.0%}}</b>
</div>
"""


# ===============================================================================

def p_binom(n, k, p, p_prime=None):
    if p_prime is None:
        p_prime = 1. - p
    return binom(n, k) * np.power(p, k) * np.power(p_prime, n - k)


def create_ticket_source(is_log):
    # Multiplicities and probabilities for each weapon tier.
    p_BT = 0.1 / 100
    p_FR = 0.5 / 100
    # p_LD = 0.75 / 100

    if is_log:
        xaxis = np.geomspace(1, 10_000, N_POINTS)
    else:
        xaxis = np.linspace(0, 1_000, N_POINTS + 1)

    df = pd.DataFrame({'n_ticket': xaxis})
    df['zero'] = 0.  # for hovertool at xaxis.
    df['n_ticket_int'] = df.n_ticket.apply(np.floor)
    df['p_BTeq0'] = np.power(1. - p_BT, df.n_ticket)
    df['p_FReq0'] = np.power(1. - p_FR, df.n_ticket)
    # df['p_LDeq0'] = np.power(1. - p_LD, df.n_ticket)
    df['p_BTgeq1'] = 1. - df['p_BTeq0']
    df['p_FRgeq1'] = 1. - df['p_FReq0']
    # df['p_LDgeq1'] = 1. - df['p_LDeq0']

    # Aiming for duplicate FR (high power stone).
    df['p_FReq1'] = p_binom(p=p_FR, n=df.n_ticket, k=1)
    df['p_FReq2'] = p_binom(p=p_FR, n=df.n_ticket, k=2)
    df['p_FReq3'] = p_binom(p=p_FR, n=df.n_ticket, k=3)
    df['p_FRgeq2'] = 1. - df['p_FReq0'] - df['p_FReq1']
    df['p_FRgeq3'] = 1. - df['p_FReq0'] - df['p_FReq1'] - df['p_FReq2']
    df['p_FRgeq4'] = 1. - df['p_FReq0'] - df['p_FReq1'] - df['p_FReq2'] - df['p_FReq3']

    return ColumnDataSource(df)


def create_gem_source(is_log):
    t_BT = 0.1 / 100
    t_FR = 0.5 / 100
    # t_LD = 0.75 / 100

    p_BT = 3.0 / 100
    p_FR = 5.0 / 100
    # p_LD = 7.5 / 100

    # 1 Multi-pull = 10 ticket + 1 boosted.
    p_BTeq0 = np.power(1. - t_BT, 10) * (1. - p_BT)
    # p_LDeq0 = np.power(1. - t_LD, 10) * (1. - p_LD)

    p_FReq0 = p_binom(p=t_FR, n=10, k=0) * (1. - p_FR)
    p_FReq1 = p_binom(p=t_FR, n=10, k=1) * (1. - p_FR) + \
              p_binom(p=t_FR, n=10, k=0) * p_FR
    p_FReq2 = p_binom(p=t_FR, n=10, k=2) * (1. - p_FR) + \
              p_binom(p=t_FR, n=10, k=1) * p_FR
    p_FReq3 = p_binom(p=t_FR, n=10, k=3) * (1. - p_FR) + \
              p_binom(p=t_FR, n=10, k=2) * p_FR

    if is_log:
        xaxis = np.geomspace(5_000, 500_000, N_POINTS)
    else:
        xaxis = np.linspace(0, 200_000, N_POINTS + 1)

    # Apply to dataframe for scenario over N attempts.
    df = pd.DataFrame({'n_gem': xaxis})
    df['zero'] = 0.  # for hovertool at xaxis.
    df['n_mtp'] = df.n_gem / 5000.
    df['n_p_int'] = df.n_mtp.apply(np.floor)

    # Aiming for duplicate FR (high power stone).
    # This is more complicate, as we can get FR either from 10 pull of smaller rate,
    # or from +1 pull of boosted rate.
    df['p_FReq0'] = np.power(p_FReq0, df.n_mtp)
    df['p_FReq1'] = p_binom(n=df.n_mtp, k=1, p=p_FReq1, p_prime=p_FReq0)
    df['p_FReq2'] = p_binom(n=df.n_mtp, k=1, p=p_FReq2, p_prime=p_FReq0) + \
                    p_binom(n=df.n_mtp, k=2, p=p_FReq1, p_prime=p_FReq0)
    df['p_FReq3'] = p_binom(n=df.n_mtp, k=1, p=p_FReq3, p_prime=p_FReq0) + \
                    binom(df.n_mtp, 2) * (binom(2, 1) * p_FReq2 * p_FReq1) * np.power(p_FReq0, df.n_mtp - 2) + \
                    p_binom(n=df.n_mtp, k=3, p=p_FReq1, p_prime=p_FReq0)

    df['p_BTeq0'] = np.power(p_BTeq0, df.n_mtp)
    # df['p_LDeq0'] = np.power(p_LDeq0, df.n_mtp)
    df['p_BTgeq1'] = 1. - df['p_BTeq0']
    df['p_FRgeq1'] = 1. - df['p_FReq0']
    df['p_FRgeq2'] = 1. - df['p_FReq0'] - df['p_FReq1']
    df['p_FRgeq3'] = 1. - df['p_FReq0'] - df['p_FReq1'] - df['p_FReq2']
    df['p_FRgeq4'] = 1. - df['p_FReq0'] - df['p_FReq1'] - df['p_FReq2'] - df['p_FReq3']
    # df['p_LDgeq1'] = 1. - df['p_LDeq0']
    return ColumnDataSource(df)


# ===============================================================================

def _create_figure(x_key, src, xtitle, is_log, label):
    """
    Create new figure with common configs.
    """
    xmin = src.data[x_key].min()
    xmax = src.data[x_key].max()
    fig = figure(
        title=TITLE,
        plot_height=HEIGHT,
        plot_width=WIDTH,
        x_range=Range1d(xmin, xmax, bounds=(xmin, xmax)),
        y_range=Range1d(bounds=(0, 1)),
        x_axis_label=xtitle,
        x_axis_type=('log' if is_log else 'auto'),
        tools='save',
        toolbar_location='above',
    )

    # Add the Series & HoverTool to the figure.
    # Last is on top. Sort by decreasing rarity.
    queues = [
        # ('p_LDgeq1', 'LD ≥ 1', 1.0, 'orange', 'solid'),
        ('p_FRgeq1', 'FR ≥ 1', 1.0, 'purple', 'solid'),
        ('p_FRgeq2', 'FR ≥ 2', 1.5, 'purple', 'solid'),
        ('p_FRgeq3', 'FR ≥ 3', 2.0, 'purple', 'solid'),
        ('p_FRgeq4', 'FR ≥ 4', 2.5, 'purple', 'solid'),
        ('p_BTgeq1', 'BT ≥ 1', 2, 'green', 'solid'),
    ]
    for field, legend, width, color, dash in queues:
        r = fig.line(x_key, field, source=src, legend_label=legend,
                     line_width=width, line_color=color, line_dash=dash)
        fig.add_tools(HoverTool(
            renderers=[r],
            tooltips=tooltip(legend, field),
            mode='vline',
        ))

    # Format axes.
    fig.xaxis.formatter = NumeralTickFormatter(format='0a')
    fig.yaxis.formatter = NumeralTickFormatter(format='0 %')
    fig.xaxis.ticker = BasicTicker(desired_num_ticks=21)
    fig.yaxis.ticker = BasicTicker(desired_num_ticks=21)

    # More horizontal line eye guide for confidence level.
    fig.ray(x=[xmin], y=[0.50], length=0, angle=0, line_width=2, color='#B0B0B0')
    fig.ray(x=[xmin], y=[0.90], length=0, angle=0, line_width=2, color='#B0B0B0')

    # Some watermark
    fig.add_layout(Label(text=label, x=10, y=0.91, x_units='screen', text_font_size='14px'))
    fig.add_layout(Label(text=URL, x=10, y=0.96, x_units='screen', text_font_size='14px'))
    fig.legend.location = 'bottom_right'
    return fig


def create_ticket_figure(is_log):
    x_key = 'n_ticket'
    src = create_ticket_source(is_log)
    label = 'Ticket Rate: LD/FR 0.5%, BT 0.1%'

    fig = _create_figure(x_key, src, 'Tickets', is_log, label)
    if is_log:
        fig.xaxis.ticker = FixedTicker(ticks=[1, 10, 20, 40, 60, 100, 200, 300, 400, 600, 1000, 2000, 5000, 9999])

    # Baseline
    r0 = fig.line(x_key, 'zero', source=src)
    fig.add_tools(HoverTool(
        renderers=[r0],
        tooltips=[('Tickets', '@n_ticket_int')],
        mode='vline',
        attachment=TooltipAttachment.below,
    ))

    return fig


def create_gem_figure(is_log):
    x_key = 'n_gem'
    src = create_gem_source(is_log)
    label = 'Gem Rate: LD/FR 0.5% (5%), BT 0.1% (3%)'

    fig = _create_figure(x_key, src, 'Gems', is_log, label)
    if is_log:
        fig.xaxis.ticker = FixedTicker(
            ticks=[500, 1e3, 2e3, 5e3, 1e4, 2e4, 3e4, 5e4, 75e3, 100e3, 125e3, 2e5, 3e5, 5e5])

    # Baseline
    r0 = fig.line(x_key, 'zero', source=src)
    fig.add_tools(HoverTool(
        renderers=[r0],
        tooltips=[('Gems', "@n_gem{0,0}"), ('Multi-pulls', '@n_p_int')],
        mode='vline',
        attachment=TooltipAttachment.below,
    ))

    # Vertical line for pity
    fig.ray(x=[75_000], y=[0], length=0, angle=np.pi / 2, line_width=1, color='orange', line_dash='dashed')
    fig.text(75_000 * 1.01, 0.01, text=["LD Pity"], color='orange', text_font_size='13px')
    fig.ray(x=[100_000], y=[0], length=0, angle=np.pi / 2, line_width=1, color='purple', line_dash='dashed')
    fig.text(100_000 * 1.01, 0.01, text=["FR Pity"], color='purple', text_font_size='13px')
    fig.ray(x=[125_000], y=[0], length=0, angle=np.pi / 2, line_width=1, color='green', line_dash='dashed')
    fig.text(125_000 * 1.01, 0.01, text=["BT Pity"], color='green', text_font_size='13px')

    return fig


# ===============================================================================

def main():
    # The figure will be rendered in a static HTML file called output_file_test.html
    output_file('rate.html', title=TITLE)

    # Create all the tabs beforehand, then just switch the visibility based on the
    # combination of the selection controls.
    panels = []
    for func in [create_gem_figure, create_ticket_figure]:
        for is_log in [True, False]:
            fig = func(is_log=is_log)
            panels.append(Panel(child=fig))
    tabs = Tabs(tabs=panels)

    # Input controls.
    currency_select = Select(title="Currency", value="Gems", options=["Gems", "Tickets"], width=96)
    log_select = Select(title='X-Axis Scale', value='Linear', options=['Log', 'Linear'], width=96)

    # Set the active tab in a binary-like logic.
    bool1 = f'(currency.value === {currency_select.options[1]!r})'
    bool2 = f'(log.value === {log_select.options[1]!r})'
    callback = CustomJS(
        args={'tabs': tabs, 'currency': currency_select, 'log': log_select},
        code=f"""tabs.active = {bool1}*2 + {bool2}""")
    currency_select.js_on_change('value', callback)
    log_select.js_on_change('value', callback)

    save(column(
        row(currency_select, log_select),
        tabs,
    ), template="""
        {% block postamble %}
        <style>
        .bk .bk-headers-wrapper {height: 0px}
        </style>
        {% endblock %}
    """)


if __name__ == '__main__':
    main()
